package com.entity;

import java.util.*;
import java.util.stream.Collectors;

public class StringStructure {
    private String line;
    public StringStructure(String line){
        this.line = line;
    }

    public String print(){
        String[] wordsArray = line.split(" ");
        List<String> words = Arrays.stream(wordsArray).filter(word -> word.length() > 1).collect(Collectors.toList());
        Set<String> letters = words.stream().map(item -> item.substring(0,1).toLowerCase()).collect(Collectors.toSet());
        List<LetterWord> letterWords = new ArrayList<>();
        for (String letter: letters){
            List<String> wordsList = words.stream().filter(item -> item.toLowerCase().startsWith(letter.toLowerCase()))
                    .collect(Collectors.toList());
            if (wordsList.size() > 1) letterWords.add(new LetterWord(letter, wordsList));
        }
        List<String> result = new ArrayList<>();

        letterWords.sort(Comparator.comparing(o -> o.getLetter().toLowerCase()));

        for (LetterWord letterWord: letterWords){
            result.add(letterWord.toString());
        }
        return result.stream().collect(Collectors.joining(", "));
    }
}
