package com.entity;

import com.sun.deploy.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class LetterWord {
    private String letter;
    private List<String> words;

    public LetterWord(String letter, List<String> words) {
        this.letter = letter;
        this.words = words;
    }

    public String getLetter() {
        return letter;
    }

    public List<String> getWords() {
        return words;
    }

    @Override
    public String toString(){
        List<String> currentWords = new ArrayList<>(words);
        currentWords.sort((o1, o2) -> {
            if (o1.length() == o2.length()) return o1.toLowerCase().compareTo(o2.toLowerCase());
            else if (o1.length() > o2.length()) return -1;
            else return 1;
        });
        return "[" + letter + "=[" + StringUtils.join(currentWords, ", ") + "]";
    }
}
