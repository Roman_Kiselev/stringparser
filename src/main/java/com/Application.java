package com;

import com.entity.StringStructure;
import com.handler.impl.KeyboardStringHandler;
import com.handler.inter.StringHandler;

import java.util.Optional;

public class Application {
    public static void main(String[] args){
        StringHandler stringHandler = new KeyboardStringHandler();
        Optional<String> string = stringHandler.get();
        string.ifPresent( item -> System.out.println(new StringStructure(item).print()));
    }
}
