package com.handler.inter;

import java.util.Optional;

public interface StringHandler {
    Optional<String> get();
}
