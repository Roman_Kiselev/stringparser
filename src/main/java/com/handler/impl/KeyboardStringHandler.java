package com.handler.impl;

import com.handler.inter.StringHandler;

import java.util.Optional;
import java.util.Scanner;

public class KeyboardStringHandler implements StringHandler {
    public Optional<String> get() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your string: ");
        return Optional.ofNullable(scanner.nextLine());
    }
}
